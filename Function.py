import pyscreenshot as ImageGrab
import pytesseract
from deep_translator import GoogleTranslator
import pyWinhook as pyHook
import pythoncom
from View import returnView
import PySimpleGUI as sg
import win32gui

def StartUI():
    openWindow(returnView())

def openWindow(window):
    loop = True
    while loop:
        #Read the event, values of the window
        event, values = window.read()

        #If the user want to quit, it quit !
        if event == sg.WINDOW_CLOSED or event == 'Quit':
            #window.close()
            break

        #If the user want to set a region
        if event == 'Set':
            setXYStartEnd()
            makeRectangle()

        #If the user want to get what is the text of the region
        if event == 'Get':
            makeScreenshot()
            window.Element('-TextIn-').Update(screenshotToText())

        #If the user want to translate what is inside the region
        if event == 'TR':
            makeScreenshot()
            text = screenshotToText()
            window.Element('-TextIn-').Update(text)
            window.Element('-TextOut-').Update(translateText(text))

        #If the user want to translate what is inside the "Input" block
        if event == 'TT':
            window.Element('-TextOut-').Update(translateText(values['-TextIn-']))

#Make a screenshot of the region and save it as temp.png
def makeScreenshot():
    im=ImageGrab.grab(bbox=(xS,yS,xE,yE))
    #im.show()
    im.save('temp.png')

#"Read" the text in the picture temp.png
def screenshotToText():
    pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'
    return pytesseract.image_to_string(r'temp.png')

#Translate a text from every source to french
def translateText(text):
    translated = GoogleTranslator(source='auto', target='fr').translate(text)
    return translated

#Event to wait when we set the region (Set xS and yS)
#xS mean coordinate x at the start of the region rectangle (top left corner)
def onMouseDownStart(event):
    global working
    working = False
    global xS
    global yS
    (xS, yS) = event.Position
    return True

#Event to wait when we set the region (Set xE and yE)
#xE mean coordinate x at the end of the region rectangle (bottom right corner)
def onMouseDownEnd(event):
    global working
    working = False
    global xE
    global yE
    (xE, yE) = event.Position
    return True

#Set the coordinate of the region rectangle
def setXYStartEnd():
    global working
    working = True

    #Add an event and wait him
    hm = pyHook.HookManager()
    hm.SubscribeMouseLeftDown(onMouseDownStart)
    hm.HookMouse()
    while(working):
        pythoncom.PumpWaitingMessages()
    hm.UnhookMouse()

    working = True
    hm = pyHook.HookManager()
    hm.SubscribeMouseLeftDown(onMouseDownEnd)
    hm.HookMouse()
    while(working):
        pythoncom.PumpWaitingMessages()
    hm.UnhookMouse()

    checkXY()

#Check if the coordinate are set in the right order
def checkXY():
    global xS
    global yS
    global xE
    global yE
    
    if(xS>xE):
        var = xS
        xS = xE
        xE = var   
    if(yS>yE):
        var = yS
        yS = yE
        yE = var  

#Show the rectangle (But it disappear really quick)
def makeRectangle():
    if(xS<0 or yS<0 or xE<0 or yE<0):
        return
    hdc = win32gui.GetDC(0)
    win32gui.DrawFocusRect(hdc, (xS, yS, xE, yE))

#Initialize global variables
def initGlobalVariables():
    global working
    working = False
    global xS
    xS = -1
    global yS
    yS = -1
    global xE
    xE = -1
    global yE
    yE = -1
