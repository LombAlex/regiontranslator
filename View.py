from tkinter.constants import CENTER, LEFT
import PySimpleGUI as sg
from win32api import GetSystemMetrics

#Return the view of the program
def returnView():
 
    #Get the width of the screen to set the window on top right
    w = GetSystemMetrics(0)

    columnOne = [
        [sg.Text("Input")],
        [sg.Multiline("Text", key='-TextIn-', size=(30, 6))],
        [sg.Text("Result")],
        [sg.Multiline("Text", key='-TextOut-', size=(30, 6))]
    ]

    columnTwo = [
        [sg.Button('Set Region', key="Set")],
        [sg.Button('Get Text', key="Get")],
        [sg.Button('Translate Region', key="TR")],
        [sg.Button('Translate Text', key="TT")],
        [sg.Button('Quit', key="Quit")]
    ]

    layout = [
        [
            sg.Column(columnOne),
            sg.Column(columnTwo)
        ]
    ]

    window = sg.Window('Window Title', layout, size=(400,300), location=(w-400,40), keep_on_top=True) #resizable=True <- not responsive

    return window